package com.engine.aw;

import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.Level;

public class CalculateRadiusMapsLatitudeLongitude {

	private static final Logger logger = Logger.getLogger(CalculateRadiusMapsLatitudeLongitude.class.getName());

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		logger.log(Level.INFO, "Input source Latitude: ");
		Scanner scannerSourceLatitude = new Scanner(System.in);
		Double sourceLatitude = scannerSourceLatitude.nextDouble();

		logger.log(Level.INFO, "Input source Longitude: ");
		Scanner scannerSourceLongitude = new Scanner(System.in);
		Double sourceLongitude = scannerSourceLongitude.nextDouble();

		logger.log(Level.INFO, "Input destination Latitude: ");
		Scanner scannerDestLatitude = new Scanner(System.in);
		Double destLatitude = scannerDestLatitude.nextDouble();

		logger.log(Level.INFO, "Input destination Longitude: ");
		Scanner scannerDestLongitude = new Scanner(System.in);
		Double destLongitude = scannerDestLongitude.nextDouble();

		Double distance = calculateDistance(sourceLatitude, sourceLongitude, destLatitude, destLongitude);
		logger.log(Level.INFO, "Radius = {0} KM", distance);
	}

	private static double calculateDistance(double latRecipient, double longRecipient, double latSmartBox,
			double longSmartBox) {
		final int R = 6371; // Radius of the earth
		double latDistance = Math.toRadians(latRecipient - latSmartBox);
		double lonDistance = Math.toRadians(longRecipient - longSmartBox);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(latRecipient))
				* Math.cos(Math.toRadians(latSmartBox)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c;
		return (Math.round(distance));
	}
}
